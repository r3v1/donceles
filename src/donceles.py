import logging
import os
import re
import time

import telebot
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.options import Options

"""
Scraper que descarga una viñeta de Don Celes
"""
logging.basicConfig(level=logging.INFO, format="%(asctime)s %(message)s",
                    handlers=[
                        logging.FileHandler("donceles.log"),
                        logging.StreamHandler()
                    ])

users = set()


def obtener_viñeta(driver, url):
    """
    Lanza el buscador headless y descarga una viñeta aleatoria de Don Celes.
    """
    src, title = None, None

    try:
        # Accede a la página
        driver.get(url)

        # Acceder a una imagen viñeta
        boton = driver.find_element_by_xpath('//li[@class="menu-item random"]')
        boton.click()

        time.sleep(5)

        # Obtener el enlace y el título de la viñeta
        image = driver.find_element_by_xpath("//a[@class='mosaic-overlay']")
        title = image.get_property('title')
        src = image.get_property('href')

    except WebDriverException as e:
        logging.info("No se ha podido descargar la viñeta: {0}".format(e))
    except ConnectionError as e:
        logging.info("No se ha podido descargar la viñeta: {0}".format(e))

    return src, title


def main():
    url = 'https://www.donceles.es/'

    # Habilitar headless
    options = Options()
    options.headless = True
    caps = DesiredCapabilities().FIREFOX
    caps['pageLoadStrategy'] = 'normal'

    # Lanzar el buscador
    driver = webdriver.Firefox(capabilities=caps, options=options, service_log_path=os.devnull)

    # Crear el bot
    bot = telebot.TeleBot(os.environ['DONCELES_TOKEN'])
    telebot.logger.setLevel(logging.INFO)  # Outputs debug messages to console.

    @bot.message_handler(commands=['start', 'help'])
    def send_welcome(message):
        bot.reply_to(message, "Soy Don Celes y te voy a enviar cada vez que me digas. Puedes pedirme "
                              "1, 2, 3, 4 o 5 viñetas, simplemente dime 'Quiero una viñeta' o 'Dame 3'"
                              "y te mandaré alguna de mis viñetas ;)")

    # Handles all text messages that match the regular expression
    @bot.message_handler(func=lambda message: True)
    def handle_message(message):
        # Registrar el usuario
        users.add(message.chat.id)

        regex = r".*([Ee]nvia(me)|[Dd]ame|[Mm]anda([mt]e)?|[Qq]uiero) ?(una|dos|tres|cuatro|cinco|[1-5])? ?(viñetas?)?"
        match = re.match(regex, message.text)
        if match:
            groups = match.groups()
            cardinal = groups[3]
            try:
                numero_vinetas = int(cardinal)
            except ValueError:
                if cardinal and cardinal != 'None':
                    if cardinal == 'una':
                        numero_vinetas = 1
                    elif cardinal == 'dos':
                        numero_vinetas = 2
                    elif cardinal == 'tres':
                        numero_vinetas = 3
                    elif cardinal == 'cuatro':
                        numero_vinetas = 4
                    elif cardinal == 'cinco':
                        numero_vinetas = 5
            except TypeError:
                bot.send_message(message.chat.id, f"Pídeme 1, 2, 3, 4 o 5 viñetas, ni más ni menos "
                                                  f"{message.from_user.first_name}")
                return

            try:
                logging.info(f"Enviando {cardinal} viñeta(s) a {message.chat.id}:{message.from_user.first_name}")
                bot.send_message(message.chat.id, f"Ahora te mando {cardinal}, espera")
                for _ in range(numero_vinetas):
                    src, title = obtener_viñeta(driver, url)
                    bot.send_photo(message.chat.id, src, title, )
                logging.info(f"Enviada(s) correctamente a {message.chat.id}:{message.from_user.first_name}")
            except:
                bot.send_message(message.chat.id, f"{message.from_user.first_name}, no he podido enviarte la(s) "
                                                  f"viñeta(s) porque no las encuentro :(")
                logging.warning(
                    f"No se ha podido enviar la viñeta a {message.chat.id}:{message.from_user.first_name}")
        else:
            logging.info(f"No se entiende el mensaje de {message.chat.id}:{message.from_user.first_name} : "
                         f"\n\t\t--> {message.text}")
            bot.send_message(message.chat.id, "No entiendo lo que me quieres decir. Si quieres una viñeta, dime "
                                              "'Envia', 'Mandame dos viñetas', 'Quiero una' o algo similiar")

    try:
        logging.info("Don Celes esta funcionando...")
        bot.polling()
    except KeyboardInterrupt:
        logging.warning("Don Celes ha dejado de actualizar los mensajes nuevos")
        bot.stop_polling()
    finally:
        logging.info("Don Celes deja de trabajar")
        for user in users:
            logging.info(f"Avisando del cierre a {user}")
            bot.send_message(user,
                             "Don Celes deja de trabajar por hoy, puedes pedirme más si quieres y te las enviaré"
                             "cuando pueda :)")
        try:
            driver.close()
        except:
            pass
        bot.stop_bot()


if __name__ == '__main__':
    main()
