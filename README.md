# Don Celes

¿Te han hecho gracia alguna vez las viñetas del mítico Don Celes? Pues ahora tienes la oportunidad de recibir una viñeta, todos los días por Telegram.

![alt text](viñeta.jpg)

## Argumento

Don Celes es un hombre de edad indeterminada, aunque indudablemente adulto, con un llamativo mostacho negro, y al igual que su mujer, Petronila Pilonga, fue bautizado por Aureliano López Becerra, director de La Gaceta del Norte. La tira es siempre muda, y su humor se basa en las desventuras de su protagonista, un antihéroe que suele terminar cada entrega golpeado, engañado o perseguido por un perro furioso.

## Historia

Don Celes Carovius, más conocido sencillamente como Don Celes, es el nombre del personaje protagonista de una tira cómica publicada diariamente por el periodista Luis del Olmo Alonso (más conocido simplemente como Olmo). Las primeras tiras cómicas de este personaje se publicaron en 1945 en el periódico La Gaceta del Norte, y posteriormente, en 1969, pasó a publicarse en la contraportada de El Correo, donde aún se publica en la actualidad, así como en otros diarios destacados, como El Diario Montañés, de Cantabria, o también El Diario Vasco.

*Fuente*: [Wikipedia](https://es.wikipedia.org/wiki/Don\_Celes)

## Requerimientos

Si quieres crear tu propio bot de Don Celes, simplemente obtén un token del *BotFather* de Telegram registrando el nombre de tu bot.

Es necesario Python 3. Luego, instala las librerías (en el sistema o en un entorno virtual) del fichero `requirements.txt`:

`pip install --user -r requirements.txt`

## Uso

Simplemente ejecuta:

`python donceles.py <token>`

## Trabajo futuro

- [ ] Recibir la viñeta cuando el usuario quiera, no a una hora determinada.
- [ ] Registrar usuarios para mejorar la experiencia de usuario? Jaja

## Autor

D. Revillas - davidrevillas2@gmail.com

